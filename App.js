import { StatusBar } from "expo-status-bar";
import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  Animated,
  PanResponder,
} from "react-native";

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

const Foods = [
  { id: "1", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "2", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "3", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "4", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "5", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "6", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "7", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "8", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "9", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "10", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "11", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "12", uri: require("./src/assets/french-food-1920x1080.jpg") },
  { id: "13", uri: require("./src/assets/french-food-1920x1080.jpg") },
];

export default function App() {
  const [currentIndex, setCurrentIndex] = useState(0);
  const pan = useRef(new Animated.ValueXY()).current;

  useEffect(() => {
    pan.setValue({ x: 0, y: 0 });
    console.log(pan.x, pan.y);
    console.log("reset");
  }, [currentIndex]);

  const rotate = pan.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: ["-10deg", "0deg", "10deg"],
    extrapolate: "clamp",
  });

  const nextCardScale = pan.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [1, 0.9, 1],
    extrapolate: "clamp",
  });

  const likeOpacity = pan.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [0, 0, 1],
    extrapolate: "clamp",
  });

  const nopeOpacity = pan.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [1, 0, 0],
    extrapolate: "clamp",
  });

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {},
      onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }], {
        useNativeDriver: false,
      }),
      onPanResponderRelease: (e, gestureState) => {
        if (gestureState.dx > 120) {
          Animated.spring(pan, {
            useNativeDriver: false,
            toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy },
            restSpeedThreshold: 1000,
            restDisplacementThreshold: 100,
          }).start(() => {
            setCurrentIndex((old) => old + 1);
          });
        } else if (gestureState.dx < -120) {
          Animated.spring(pan, {
            useNativeDriver: false,
            toValue: { x: -SCREEN_WIDTH - 100, y: gestureState.dy },
            restSpeedThreshold: 1000,
            restDisplacementThreshold: 100,
          }).start(() => {
            setCurrentIndex((old) => old + 1);
          });
        } else {
          Animated.spring(pan, {
            useNativeDriver: false,
            toValue: { x: 0, y: 0 },
            friction: 4,
          }).start();
        }
      },
    })
  ).current;

  renderFoods = () => {
    return Foods.map((item, i) => {
      if (i < currentIndex) {
        return null;
      } else if (i == currentIndex) {
        return (
          <Animated.View
            {...panResponder.panHandlers}
            key={i}
            style={{
              height: SCREEN_HEIGHT - 120,
              width: SCREEN_WIDTH,
              padding: 10,
              position: "absolute",
              transform: [
                { rotate: rotate },
                { translateX: pan.x },
                { translateY: pan.y },
              ],
            }}
          >
            <Animated.View
              style={{
                transform: [{ rotate: "-30deg" }],
                position: "absolute",
                opacity: likeOpacity,
                top: 50,
                left: 40,
                zIndex: 1000,
              }}
            >
              <Text
                style={{
                  borderWidth: 1,
                  borderColor: "green",
                  color: "green",
                  fontSize: 38,
                  fontWeight: "bold",
                  padding: 10,
                }}
              >
                LIKE
              </Text>
            </Animated.View>
            <Animated.View
              style={{
                transform: [{ rotate: "30deg" }],
                position: "absolute",
                opacity: nopeOpacity,
                top: 50,
                right: 40,
                zIndex: 1000,
              }}
            >
              <Text
                style={{
                  borderWidth: 1,
                  borderColor: "red",
                  color: "red",
                  fontSize: 38,
                  fontWeight: "bold",
                  padding: 10,
                }}
              >
                NOPE
              </Text>
            </Animated.View>
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "cover",
                borderRadius: 20,
              }}
              source={item.uri}
            />
          </Animated.View>
        );
      } else {
        return (
          <Animated.View
            key={i}
            style={{
              height: SCREEN_HEIGHT - 120,
              width: SCREEN_WIDTH,
              padding: 10,
              position: "absolute",
              transform: [{ scale: nextCardScale }],
            }}
          >
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "cover",
                borderRadius: 20,
              }}
              source={item.uri}
            />
          </Animated.View>
        );
      }
    }).reverse();
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: 60 }}></View>
      <View style={{ flex: 1 }}>{renderFoods()}</View>
      <View style={{ height: 60 }}></View>
    </View>
  );
}
